package main

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/tealeg/xlsx/v3"

	"gitlab.com/jkmn/jlcpcb/pkg/util"
)

func parseBOM() []Material {
	mats := make([]Material, 0)

	var rows [][]string

	if strings.HasSuffix(*bomPath, "xlsx") {
		wb, err := xlsx.OpenFile(*bomPath)
		if err != nil {
			panic(err)
		}

		if len(wb.Sheets) == 0 {
			log.Fatal("The BOM file contains no sheets.")
		}
		xlsxSheet := wb.Sheets[0]

		rows = util.SimplifSheet(xlsxSheet)
	}

	if strings.HasSuffix(*bomPath, "csv") {
		f, err := os.Open(*bomPath)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()

		rdr := csv.NewReader(f)
		rdr.FieldsPerRecord = -1

		rows, err = rdr.ReadAll()
		if err != nil {
			log.Fatal(err)
		}
	}

	var headerMap map[string]int
	var headerIdx int
	for i, row := range rows {
		if row[0] == "Ref" {
			headerMap = parseBomHeaders(row)
			headerIdx = i
			break
		}
	}

	for i := headerIdx + 1; i < len(rows); i++ {
		row := rows[i]
		part := parseBomRow(row, headerMap)
		if part == nil {
			log.Printf("invalid material on row %d", i+1)
			return mats
		}
		mats = append(mats, *part)

	}

	return mats
}

func parseBomHeaders(row []string) map[string]int {
	headerMap := make(map[string]int)
	for i := 0; i < len(row); i++ {
		s := row[i]
		switch s {
		case ColRef:
			headerMap[ColRef] = i
		case ColLcscPartNumber:
			headerMap[ColLcscPartNumber] = i
		case ColQnty:
			headerMap[ColQnty] = i
		case ColValue:
			headerMap[ColValue] = i
		case ColCmpName:
			headerMap[ColCmpName] = i
		case ColFootprint:
			headerMap[ColFootprint] = i
		case ColDescription:
			headerMap[ColDescription] = i
		case ColVendor:
			headerMap[ColVendor] = i
		case "":
			return headerMap
		}
	}
	return headerMap
}

const (
	ColRef            = "Ref"
	ColLcscPartNumber = "LCSC part number"
	ColQnty           = "Qnty"
	ColValue          = "Value"
	ColCmpName        = "Cmp name"
	ColFootprint      = "Footprint"
	ColDescription    = "Description"
	ColVendor         = "Vendor"
)

func parseBomRow(row []string, headerMap map[string]int) *Material {
	mat := &Material{}
	var err error

	for header, cellIdx := range headerMap {
		if cellIdx >= len(row) {
			continue
		}

		v := row[cellIdx]

		switch header {
		case ColRef:
			chunks := strings.Split(v, ",")
			for _, chunk := range chunks {
				chunk = strings.TrimSpace(chunk)
				if len(chunk) > 0 {
					mat.References = append(mat.References, chunk)
				}
			}
		case ColLcscPartNumber:
			mat.LcscPartNo = v
		case ColQnty:
			mat.Qty, err = strconv.Atoi(v)
			if err != nil {
				log.Print(err)
			}
		case ColValue:
			mat.Value = v
		case ColCmpName:
			mat.Name = v
		case ColFootprint:
			mat.Footprint = util.SimplifyFootprint(v)
		case ColDescription:
			mat.Description = v
		case ColVendor:
			mat.Vendor = v
		}

	}

	if len(mat.References) == 0 {
		return nil
	}

	return mat
}

type Material struct {
	References  []string
	LcscPartNo  string
	Qty         int
	Value       string
	Name        string
	Footprint   string
	Description string
	Vendor      string
}
