package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/jung-kurt/gofpdf"
	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/jlcpcb/pkg/digikey"
)

var (
	//PPI           float64 = gofpdf. // Default points per inch
	CARD_WIDTH    float64 = 2.66666666 // Width of the card
	CARD_HEIGHT   float64 = 1.75       // Height of the card
	CARD_PADDING  float64 = 0.125      // Padding between the content and the card edge
	MARGIN_COL    float64 = 0          // Space between the columns of cards
	MARGIN_LEFT   float64 = 0.25       // Left margin of the card page
	MARGIN_TOP    float64 = 0.25       // Top margin of the card page
	CARDS_PER_ROW int     = 3
	CARDS_PER_COL int     = 6
)

var (
	bomPath *string = flag.String("bom", "", "BOM to process")
	qty     *int    = flag.Int("qty", 1, "quantity to print")
	outPath *string = flag.String("o", "", "file to write")
)

func main() {
	flag.Parse()
	if *bomPath == "" {
		flag.Usage()
		log.Print("Please provide the path to a BOM file to process.")
		os.Exit(1)
	}
	if *qty < 1 {
		flag.Usage()
		log.Print("Please provide a quantity greater than 1.")
		os.Exit(1)
	}

	var err error

	parts, err := digikey.ParseBOM(*bomPath)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	//jlog.Log(parts)

	pdf := gofpdf.New("P", "in", "Letter", "")
	pdf.SetFont("Arial", "", 10)
	pdf.SetMargins(0, 0, 0)

	err = fillPartsCards(pdf, parts, *qty)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	if *outPath == "" {
		*outPath = *bomPath + ".cards.pdf"
	}

	err = pdf.OutputFileAndClose(*outPath)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}
}

func fillCards(c *gofpdf.Fpdf, contents []string) error {

	var cardsPerPage = CARDS_PER_COL * CARDS_PER_ROW
	for i, content := range contents {
		// This creates a new page.
		if i%cardsPerPage == 0 {
			c.AddPage()
			drawBorders(c)
		}
		col := (i % cardsPerPage) % (CARDS_PER_ROW)
		row := (i % cardsPerPage) / CARDS_PER_ROW

		var (
			// These should reflect the top left corner of the physical label.
			x float64 = MARGIN_LEFT + CARD_WIDTH*float64(col) + MARGIN_COL*float64(col)
			y float64 = MARGIN_TOP + CARD_HEIGHT*float64(row)

			// These represent the top left corner of the printable area of the label.
			x_ float64 = x + CARD_PADDING
			y_ float64 = y + CARD_PADDING
			w  float64 = CARD_WIDTH - 2*CARD_PADDING
		)

		//log.Printf("col: %d (%f)\trow: %d (%f)", col, x, row, y)

		// Finally, draw the paragraph in the proper location.
		c.SetXY(x_, y_)
		c.MultiCell(w, 0.20, content, "", "L", false)
	}

	return nil
}

func drawBorders(c *gofpdf.Fpdf) {

	// Draw a lines around cards for debugging
	var (
		x1, x2, y1, y2 float64
		w, h           float64 = c.GetPageSize()
	)

	for x := MARGIN_LEFT; x <= w; x += CARD_WIDTH {
		x1 = x
		x2 = x
		y1 = 0
		y2 = h
		c.Line(x1, y1, x2, y2)
	}

	for y := MARGIN_TOP; y <= h; y += CARD_HEIGHT {
		x1 = 0
		x2 = w
		y1 = y
		y2 = y
		c.Line(x1, y1, x2, y2)
	}
}

func fillPartsCards(c *gofpdf.Fpdf, parts []digikey.Part, qty int) error {
	contents := make([]string, 0)
	for _, part := range parts {
		for i := 0; i < qty; i++ {
			b := bytes.NewBuffer(nil)
			fmt.Fprintf(b, "DigiKey Part: %s\n", part.DigiKeyPartNumber)
			fmt.Fprintf(b, "Description: %s\n", part.Description)
			fmt.Fprintf(b, "Quantity: %d\n", part.Quantity)
			//fmt.Fprintf(b, "References: %s\n", part.ReferenceDesignator)
			contents = append(contents, b.String())
		}
	}
	return fillCards(c, contents)
}
