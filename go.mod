module gitlab.com/jkmn/jlcpcb

go 1.14

require (
	github.com/jung-kurt/gofpdf v1.16.2
	github.com/tealeg/xlsx/v3 v3.2.0
	gitlab.com/jkmn/errors v0.0.0-20190223191516-b7e3ad5694c7
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
