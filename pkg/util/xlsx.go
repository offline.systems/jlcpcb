package util

import (
	"log"

	"github.com/tealeg/xlsx/v3"
)

func SimplifSheet(sheet *xlsx.Sheet) [][]string {
	rows := make([][]string, 0)

	for i := 0; i <= sheet.MaxRow; i++ {
		row, err := sheet.Row(i)
		if err != nil {
			log.Fatal("unexpected error: ", err)
		}

		if row.GetCell(0).String() == "" {
			return rows
		}

		maxCol := 0
		for j := 0; j <= sheet.MaxCol; j++ {
			if row.GetCell(j).String() != "" {
				maxCol = j
			}
		}

		row_ := make([]string, 0)
		for j := 0; j <= maxCol; j++ {
			row_ = append(row_, row.GetCell(j).String())
		}

		rows = append(rows, row_)
	}

	return rows
}

