package util

import "strings"

func SimplifyFootprint(s string) string {

	chunks := strings.Split(s,":")
	s = chunks[len(chunks)-1]

	if strings.HasPrefix(s,"L_"){
		s = s[2:]
	}
	if strings.HasPrefix(s,"R_"){
		s = s[2:]
	}
	if strings.HasPrefix(s,"C_"){
		s = s[2:]
	}

	switch s {
	case "SOIC-8_3.9x4.9x1.27P":
		return "SOIC-8"
	case "SOIC-14_3.9x8.7mm_Pitch1.27mm":
		return "SOIC-14"
	case "HTSSOP-16_EP_4.4x5.0x0.65P":
		return "SSOP-16"
	case "SOIC-16_3.9x9.9mm_Pitch1.27mm":
		return "SOIC-16"
	case "SOIC-14_3.9x8.7x1.27P":
		return "SOIC-14"
	case "SOIC-8_3.9x4.9mm_Pitch1.27mm":
		return "SOIC-8"
	}

	return s
}

