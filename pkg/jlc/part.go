package jlc

type Part struct {
	LcscPart       string
	FirstCategory  string
	SecondCategory string
	MfrPart        string
	Package        string
	SolderJoint    int
	Manufacturer   string
	LibraryType    string
	Description    string
	Datasheet      string
	Price          map[int]float64
	Stock          int
}

func (p *Part) PriceForQty(qty int) float64 {
	var (
		breakQty   int = -1
		breakPrice float64
	)

	for q, p := range p.Price {
		if breakQty == -1 || (q < breakQty && qty >= q) {
			breakQty = q
			breakPrice = p
		}
	}

	return breakPrice
}

func (p *Part) LowQtyPrice() float64 {
	var (
		breakQty   int = -1
		breakPrice float64
	)

	for q, p := range p.Price {
		if breakQty == -1 || q < breakQty {
			breakQty = q
			breakPrice = p
		}
	}

	return breakPrice
}
