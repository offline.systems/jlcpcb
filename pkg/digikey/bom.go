package digikey

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/tealeg/xlsx/v3"
	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/jlcpcb/pkg/util"
)

func ParseBOM(path string) ([]Part, error) {
	mats := make([]Part, 0)

	var rows [][]string

	if strings.HasSuffix(path, "xlsx") {
		wb, err := xlsx.OpenFile(path)
		if err != nil {
			return nil, errors.Stack(err)
		}

		if len(wb.Sheets) == 0 {
			return nil, errors.New("BOM file contains no sheets")
		}
		xlsxSheet := wb.Sheets[0]

		rows = util.SimplifSheet(xlsxSheet)
	}

	if strings.HasSuffix(path, "csv") {
		f, err := os.Open(path)
		if err != nil {
			return nil, errors.Stack(err)
		}
		defer f.Close()

		rdr := csv.NewReader(f)

		rows, err = rdr.ReadAll()
		if err != nil {
			return nil, errors.Stack(err)
		}
	}

	var headerMap map[string]int
	var headerIdx int
	for i, row := range rows {
		if row[0] == "Manufacturer Part Number" {
			headerMap = parseBomHeaders(row)
			headerIdx = i
			break
		}
	}

	for i := headerIdx + 1; i < len(rows); i++ {
		row := rows[i]
		part := parseBomRow(row, headerMap)
		if part == nil {
			log.Printf("invalid material on row %d", i+1)
			return mats, nil
		}
		mats = append(mats, *part)

	}

	return mats, nil
}

func parseBomHeaders(row []string) map[string]int {
	headerMap := make(map[string]int)
	for i := 0; i < len(row); i++ {
		s := row[i]
		switch s {
		case ColManufacturerPartNumber:
			headerMap[ColManufacturerPartNumber] = i
		case ColManufacturer:
			headerMap[ColManufacturer] = i
		case ColDigiKeyPartNumber:
			headerMap[ColDigiKeyPartNumber] = i
		case ColCustomerReference:
			headerMap[ColCustomerReference] = i
		case ColReferenceDesignator:
			headerMap[ColReferenceDesignator] = i
		case ColPackaging:
			headerMap[ColPackaging] = i
		case ColPartStatus:
			headerMap[ColPartStatus] = i
		case ColQuantity:
			headerMap[ColQuantity] = i
		case ColUnitPrice:
			headerMap[ColUnitPrice] = i
		case ColExtendedPrice:
			headerMap[ColExtendedPrice] = i
		case ColQuantityAvailable:
			headerMap[ColQuantityAvailable] = i
		case ColMfgStdLeadTime:
			headerMap[ColMfgStdLeadTime] = i
		case ColDescription:
			headerMap[ColDescription] = i
		case ColRoHsStatus:
			headerMap[ColRoHsStatus] = i
		case ColLeadFreeStatus:
			headerMap[ColLeadFreeStatus] = i
		case ColReachStatus:
			headerMap[ColReachStatus] = i
		case "":
			return headerMap
		}
	}
	return headerMap
}

const (
	ColManufacturerPartNumber = "Manufacturer Part Number"
	ColManufacturer           = "Manufacturer"
	ColDigiKeyPartNumber      = "Digi-Key Part Number"
	ColCustomerReference      = "Customer Reference"
	ColReferenceDesignator    = "Reference Designator"
	ColPackaging              = "Packaging"
	ColPartStatus             = "Part Status"
	ColQuantity               = "Quantity"
	ColUnitPrice              = "Unit Price"
	ColExtendedPrice          = "Extended Price"
	ColQuantityAvailable      = "Quantity Available"
	ColMfgStdLeadTime         = "Mfg Std Lead Time"
	ColDescription            = "Description"
	ColRoHsStatus             = "RoHS Status"
	ColLeadFreeStatus         = "Lead Free Status"
	ColReachStatus            = "REACH Status"
)

func parseBomRow(row []string, headerMap map[string]int) *Part {
	part := &Part{}
	//var err error

	for header, cellIdx := range headerMap {
		if cellIdx >= len(row) {
			continue
		}

		v := row[cellIdx]

		switch header {
		case ColManufacturerPartNumber:
			part.ManufacturerPartNumber = v
		case ColManufacturer:
			part.Manufacturer = v
		case ColDigiKeyPartNumber:
			part.DigiKeyPartNumber = v
		case ColCustomerReference:
			part.CustomerReference = v
		case ColReferenceDesignator:
			part.ReferenceDesignator = v
		case ColPackaging:
			part.Packaging = v
		case ColPartStatus:
			part.PartStatus = v
		case ColQuantity:
			part.Quantity, _ = strconv.Atoi(v)
		case ColUnitPrice:
			part.UnitPrice = v
		case ColExtendedPrice:
			part.ExtendedPrice = v
		case ColQuantityAvailable:
			part.QuantityAvailable = v
		case ColMfgStdLeadTime:
			part.MfgStdLeadTime = v
		case ColDescription:
			part.Description = v
		case ColRoHsStatus:
			part.RoHsStatus = v
		case ColLeadFreeStatus:
			part.LeadFreeStatus = v
		case ColReachStatus:
			part.ReachStatus = v
		}

	}

	return part
}

type Part struct {
	ManufacturerPartNumber string
	Manufacturer           string
	DigiKeyPartNumber      string
	CustomerReference      string
	ReferenceDesignator    string
	Packaging              string
	PartStatus             string
	Quantity               int
	UnitPrice              string
	ExtendedPrice          string
	QuantityAvailable      string
	MfgStdLeadTime         string
	Description            string
	RoHsStatus             string
	LeadFreeStatus         string
	ReachStatus            string
}
